package com.example.demo.repository;

import com.example.demo.model.entity.EmployeeEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository extends CrudRepository<EmployeeEntity,Long> {
    List<EmployeeEntity> findByFirstname(String name);
    Optional<EmployeeEntity> findByFirstnameAndAgeGreaterThan(String name,Long age);
    List<EmployeeEntity> findEmployeeEntitiesBySalaryGreaterThan(Long input);
}
