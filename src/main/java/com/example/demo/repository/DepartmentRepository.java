package com.example.demo.repository;

import com.example.demo.model.entity.DepartmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DepartmentRepository extends JpaRepository<DepartmentEntity,Long> {

    List<DepartmentEntity> findDepartmentEntitiesByNameStartsWith(String input);
}
