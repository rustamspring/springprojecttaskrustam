package com.example.demo.model.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EmployeeDto {
    //COMMENT
    Long id;
    String firstname;
    String lastname;
    Long age;
    Long salary;
}
