package com.example.demo.model.dto.fullDto;


import com.example.demo.model.dto.AddressDto;
import com.example.demo.model.dto.DepartmentDto;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FullEmployeeDto {
    String firstname;
    String lastname;
    Long age;
    Long salary;
    DepartmentDto departmentDto;
    AddressDto addressDto;
}
