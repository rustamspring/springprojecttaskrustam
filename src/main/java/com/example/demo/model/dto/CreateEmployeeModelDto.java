package com.example.demo.model.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateEmployeeModelDto {
    Long id;
    String firstname;
    String lastname;
    Long age;
    Long salary;
}
