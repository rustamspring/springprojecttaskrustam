package com.example.demo.service.impl;

import com.example.demo.model.dto.DepartmentDto;
import com.example.demo.model.dto.fullDto.FullDepartmentDto;
import com.example.demo.model.dto.fullDto.FullEmployeeDto;
import com.example.demo.model.entity.DepartmentEntity;
import com.example.demo.model.entity.EmployeeEntity;
import com.example.demo.repository.DepartmentRepository;
import com.example.demo.service.serviceInterfaces.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepository departmentRepository;

    @Override
    public List<FullDepartmentDto> getAllDepartments() {
        return departmentRepository.findAll().stream()
                .map(data -> FullDepartmentDto.builder()
                        .name(data.getName())
                        .fullEmployeeDto(data.getEmployeeEntity()
                                .stream()
                                .map(emp -> FullEmployeeDto.builder()
                                        .firstname(emp.getFirstname())
                                        .lastname(emp.getLastname())
                                        .age(emp.getAge())
                                        .salary(emp.getSalary())
                                        .build()).toList())
                        .build()).toList();

    }

    @Override
    public Boolean addDepartment(FullDepartmentDto fullDepartmentDto) {
        var depEntity = DepartmentEntity.builder()
                .name(fullDepartmentDto.getName())
                .employeeEntity(fullDepartmentDto.getFullEmployeeDto().stream()
                        .map(employeeDto -> EmployeeEntity.builder()
                                .age(employeeDto.getAge())
                                .salary(employeeDto.getSalary())
                                .firstname(employeeDto.getFirstname())
                                .lastname(employeeDto.getLastname())
                                .build())
                        .toList())
                .build();
        if (depEntity != null) {
            departmentRepository.save(depEntity);
            return true;
        }
        return false;
    }
    @Override
    public List<DepartmentDto> getDepartmentsStartWith(String letter) {
        var entity = departmentRepository.findDepartmentEntitiesByNameStartsWith(letter);

        return entity.stream().map(data -> DepartmentDto.builder()
                .name(data.getName())
                .build()).toList();
    }
}
