package com.example.demo.service.impl;


import com.example.demo.model.dto.AddressDto;
import com.example.demo.model.dto.DepartmentDto;
import com.example.demo.model.dto.fullDto.FullDepartmentDto;
import com.example.demo.model.dto.fullDto.FullEmployeeDto;
import com.example.demo.model.entity.AddressEntity;
import com.example.demo.model.entity.DepartmentEntity;
import com.example.demo.model.entity.EmployeeEntity;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.service.serviceInterfaces.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("employeeService")
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;


    @Override
    public List<FullEmployeeDto> getAll() {
        List<EmployeeEntity> list = new ArrayList<>();

        employeeRepository.findAll().forEach(list::add);
        List<FullEmployeeDto> fullEmployeeDtoList = new ArrayList<>();
        list.forEach(data -> fullEmployeeDtoList.add(FullEmployeeDto.builder()
                .firstname(data.getFirstname())
                .lastname(data.getLastname())
                .salary(data.getSalary())
                .addressDto(AddressDto.builder()
                        .address_name(data.getAddressEntity().getAddressName())
                        .build())
                .age(data.getAge())
                .departmentDto(DepartmentDto.builder()
                        .name(data.getDepartmentEntity().getName())
                        .build())
                .build()));
        return fullEmployeeDtoList;
    }

    @Override
    public FullEmployeeDto getById(Long id) {
        var employeeobj = employeeRepository.findById(id);
        if (employeeobj.isEmpty()) {
            System.err.println("Employee Tapilmadi " + id);
            return null;
        }
        return FullEmployeeDto.builder()
                .firstname(employeeobj.get().getFirstname())
                .lastname(employeeobj.get().getLastname())
                .age(employeeobj.get().getAge())
                .salary(employeeobj.get().getSalary())
                .addressDto(AddressDto.builder()
                        .address_name(employeeobj.get().getAddressEntity().getAddressName())
                        .build())
                .departmentDto(DepartmentDto.builder()
                        .name(employeeobj.get().getDepartmentEntity().getName())
                        .build())
                .build();

    }


    @Override
    public Long addEmployee(FullEmployeeDto fullEmployeeDto) {
        EmployeeEntity employee = EmployeeEntity.builder()
                .age(fullEmployeeDto.getAge())
                .firstname(fullEmployeeDto.getFirstname())
                .lastname(fullEmployeeDto.getLastname())
                .salary(fullEmployeeDto.getSalary())
                .departmentEntity(DepartmentEntity.builder()
                        .name(fullEmployeeDto.getDepartmentDto().getName())
                        .build())
                .addressEntity(AddressEntity.builder()
                        .addressName(fullEmployeeDto.getAddressDto().getAddress_name())
                        .build())
                .build();
        var employeeEntity = employeeRepository.save(employee);
        return employeeEntity.getId();
    }

    @Override
    public void deleteEmployeeById(Long id) {
        employeeRepository.deleteById(id);
    }

    @Override
    public FullEmployeeDto updateById(Long id, FullEmployeeDto fullEmployeeDto) {
        var employeeEntity = employeeRepository.findById(id);
        if (employeeEntity.isPresent()) {
            employeeEntity.get().setAge(fullEmployeeDto.getAge());
            employeeEntity.get().setFirstname(fullEmployeeDto.getFirstname());
            employeeEntity.get().setLastname(fullEmployeeDto.getLastname());
            employeeEntity.get().setDepartmentEntity(DepartmentEntity.builder()
                    .name(fullEmployeeDto.getDepartmentDto().getName())
                    .build());
            employeeEntity.get().setAddressEntity(AddressEntity.builder()
                    .addressName(fullEmployeeDto.getAddressDto().getAddress_name())
                    .build());
            employeeEntity.get().setSalary(fullEmployeeDto.getSalary());
            employeeRepository.save(employeeEntity.get());
        }

        return fullEmployeeDto.builder()
                .firstname(employeeEntity.get().getFirstname())
                .lastname(employeeEntity.get().getLastname())
                .salary(employeeEntity.get().getSalary())
                .addressDto(AddressDto.builder()
                        .address_name(employeeEntity.get().getFirstname())
                        .build())
                .departmentDto(DepartmentDto.builder()
                        .name(employeeEntity.get().getDepartmentEntity().getName())
                        .build())
                .age(employeeEntity.get().getAge()).build();
    }

    @Override
    public List<FullEmployeeDto> getEmployeeByName(String name) {
        List<EmployeeEntity> empl = employeeRepository.findByFirstname(name);
        if (empl.isEmpty()) {
            return null;
        }
        return empl.stream().map(data -> FullEmployeeDto.builder()
                .firstname(data.getFirstname())
                .salary(data.getSalary())
                .lastname(data.getLastname())
                .age(data.getAge())
                .departmentDto(DepartmentDto.builder()
                        .name(data.getDepartmentEntity().getName())
                        .build())
                .addressDto(AddressDto.builder()
                        .address_name(data.getAddressEntity().getAddressName())
                        .build())
                .build()).toList();

    }

    @Override
    public List<FullEmployeeDto> getEmployeeBySalaryGreaterThan(Long salary) {
        return employeeRepository.findEmployeeEntitiesBySalaryGreaterThan(salary)
                .stream()
                .map(data -> FullEmployeeDto.builder()
                        .age(data.getAge())
                        .lastname(data.getFirstname())
                        .firstname(data.getFirstname())
                        .salary(data.getSalary())
                        .addressDto(AddressDto.builder()
                                .address_name(data.getAddressEntity().getAddressName())
                                .build())
                        .build()).toList();
    }
}
