package com.example.demo.service.impl;

import com.example.demo.model.dto.AddressDto;
import com.example.demo.model.entity.AddressEntity;
import com.example.demo.repository.AddressRepository;
import com.example.demo.service.serviceInterfaces.AddressService;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {
    private final AddressRepository addressRepository;

    @Override
    public AddressDto getAddressById(Long id) {
        var address = addressRepository.findById(id);
        if (address.isEmpty()) {
            System.err.println("Address Tapilmadi " + id);
            return null;
        }
        return AddressDto.builder()
                .address_name(address.get().getAddressName())
                .build();
    }

    @Override
    public List<AddressDto> getAllAddresses() {
        return addressRepository.findAll().stream().map(data -> AddressDto.builder()
                .address_name(data.getAddressName())
                .build()).toList();
    }

    @Override
    public Long deleteAddress(Long id) {
        Optional<AddressEntity> address = addressRepository.findById(id);
        if (address.isPresent()) {
            addressRepository.deleteById(id);
            return 0L;
        }
        return 1L;
    }

    @Override
    public Long updateAddressById(Long id, AddressDto addressDto) {
        var address = addressRepository.findById(id);
        if (address.isPresent()) {
            address.get().setAddressName(addressDto.getAddress_name());
            addressRepository.save(address.get());
            return 0L;
        }
        return 1L;
    }

    @Override
    public Long createAddress(AddressDto addressDto) {
        var address = AddressEntity.builder()
                .addressName(addressDto.getAddress_name())
                .build();
        addressRepository.save(address);
        return 0L;
    }
}
