package com.example.demo.service.impl;

import com.example.demo.exception.FileNotFoundException;
import com.example.demo.model.dto.FileReq;
import com.example.demo.service.seriveInterfaces.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@Slf4j
@Service
public class FileServiceImpl implements FileService {

    @Override
    public void getFileInfo(MultipartFile file) {
        if(file.isEmpty()){
            throw new FileNotFoundException("File is empty");
        }
        log.info(file.getName());
        log.info(file.getOriginalFilename());
        log.info(file.getContentType());
    }

    @Override
    public void getFileInfoV2(FileReq fileReq) {
        if(fileReq.getFile().isEmpty()){
            throw new FileNotFoundException("File is empty");
        }
        log.info(fileReq.getFileName());
        log.info(fileReq.getFolderName());
        log.info(fileReq.getFile().getContentType());
    }

    public ResponseEntity<String> uploadFile(FileReq fileReq) {
        var file = fileReq.getFile();
        log.info(file.getName());
        log.info(file.getOriginalFilename());
        log.info(file.getContentType());
        Path destination = Paths.get("rootDir")
                .resolve(Objects.requireNonNull(file.getOriginalFilename()))
                .normalize().toAbsolutePath();
        try {
            if (file.isEmpty()) {
                throw new FileNotFoundException("Empty file");
            }
            Files.copy(file.getInputStream(), destination);
        } catch (IOException e) {
            throw new RuntimeException("Store exception");
        }
        log.info(file.getContentType());
        return new ResponseEntity<>(destination.toString(), HttpStatus.OK);
    }
}

