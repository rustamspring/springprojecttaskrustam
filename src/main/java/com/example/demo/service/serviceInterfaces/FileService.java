package com.example.demo.service.seriveInterfaces;

import com.example.demo.model.dto.FileReq;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {
    public void getFileInfo(MultipartFile file);
    public void getFileInfoV2(FileReq filereq);
    public ResponseEntity<String> uploadFile(FileReq fileReq);
}

