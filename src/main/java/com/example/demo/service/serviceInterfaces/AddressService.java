package com.example.demo.service.serviceInterfaces;

import com.example.demo.model.dto.AddressDto;

import java.util.List;

public interface AddressService {
    AddressDto getAddressById(Long id);

    List<AddressDto> getAllAddresses();

    Long deleteAddress(Long id);

    Long updateAddressById(Long id,AddressDto addressDto);

    Long createAddress(AddressDto addressDto);
}
