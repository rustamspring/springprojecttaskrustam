package com.example.demo.service.serviceInterfaces;

import com.example.demo.model.dto.CreateEmployeeModelDto;
import com.example.demo.model.dto.EmployeeDto;
import com.example.demo.model.dto.fullDto.FullEmployeeDto;

import java.util.List;

public interface EmployeeService {
        List<FullEmployeeDto> getAll();
        FullEmployeeDto getById(Long id);
        Long addEmployee(FullEmployeeDto fullEmployeeDto);

        void deleteEmployeeById(Long id);
        FullEmployeeDto updateById(Long id , FullEmployeeDto fullEmployeeDto);

        List<FullEmployeeDto> getEmployeeByName(String name);

        List<FullEmployeeDto> getEmployeeBySalaryGreaterThan(Long salary);
}
