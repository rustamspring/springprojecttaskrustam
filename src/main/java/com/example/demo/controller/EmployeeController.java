package com.example.demo.controller;


import com.example.demo.model.dto.CreateEmployeeModelDto;
import com.example.demo.model.dto.EmployeeDto;
import com.example.demo.model.dto.fullDto.FullEmployeeDto;
import com.example.demo.service.serviceInterfaces.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.aspectj.weaver.loadtime.definition.LightXMLParser;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/emp")
@RequiredArgsConstructor
@Qualifier(value = "employeeService")
public class EmployeeController {


    private final EmployeeService employeeService;


    @GetMapping("/getAll")
    @ResponseStatus(HttpStatus.OK)
    public List<FullEmployeeDto> getAll() {
        return employeeService.getAll();
    }

    @GetMapping("/getById")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<FullEmployeeDto> getById(@RequestParam("id") Long id) {
        if (employeeService.getById(id) == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_EXTENDED);
        }
        return new ResponseEntity<>(employeeService.getById(id), HttpStatus.OK);
    }

    @GetMapping("/getByName")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<FullEmployeeDto>> getByName(@RequestParam("name") String name) {
        var query = employeeService.getEmployeeByName(name);
        if (query.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_EXTENDED);

        }
        return new ResponseEntity<>(query, HttpStatus.OK);
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.OK)
    public Long addEmployee(@RequestBody FullEmployeeDto fullEmployeeDto) {
        return employeeService.addEmployee(fullEmployeeDto);
    }

    @DeleteMapping("/deleteById")
    @ResponseStatus(HttpStatus.OK)
    public void deleteEmployeeById(@RequestParam("id") Long id) {
        employeeService.deleteEmployeeById(id);
    }

    @PutMapping("/updateById")
    @ResponseStatus(HttpStatus.OK)
    public FullEmployeeDto updateEmployeeById(@RequestParam("id") Long id, @RequestBody FullEmployeeDto fullEmployeeDto) {
        return employeeService.updateById(id, fullEmployeeDto);
    }
    @GetMapping("/getEmployeeBySalaryGreatenThan")
    @ResponseStatus(HttpStatus.OK)
    public List<FullEmployeeDto> getEmployeeBySalaryGreaterThan(@RequestParam("salary") Long salary){
        return employeeService.getEmployeeBySalaryGreaterThan(salary);
    }
}

