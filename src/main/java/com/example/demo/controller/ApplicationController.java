package com.example.demo.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/config")
public class ApplicationController {

    public final String USERNAME ="username";
    public final String  PASSWORD ="password";

    //@Value("${myDatabase.username}")
    private String username;
    //@Value("${myDatabase.password}")
    private String password;

    @GetMapping
    public HashMap<String, String> getConfigProperties() {
        var map = new HashMap<String,String>();
        map.put(USERNAME, username);
        map.put(PASSWORD, password);
        return map;
    }
}
