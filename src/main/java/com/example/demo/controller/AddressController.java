package com.example.demo.controller;

import com.example.demo.model.dto.AddressDto;
import com.example.demo.service.serviceInterfaces.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/address")
public class AddressController {
    private final AddressService addressService;

    @GetMapping("/getAddressById")
    @ResponseStatus(HttpStatus.OK)
    AddressDto getAddressById(@RequestParam("id") Long id) {
        return addressService.getAddressById(id);
    }

    @GetMapping("/getAllAddresses")
    @ResponseStatus(HttpStatus.OK)
    List<AddressDto> getAllAddresses() {
        return addressService.getAllAddresses();
    }

    @DeleteMapping("/deleteById")
    @ResponseStatus(HttpStatus.OK)
    Long deleteAddress(@RequestParam("id") Long id) {

        return addressService.deleteAddress(id);
    }

    @PutMapping("/updateAddressById")
    @ResponseStatus(HttpStatus.OK)
    Long updateAddressById(@RequestParam("id") Long id, @RequestBody AddressDto addressDto) {
        return addressService.updateAddressById(id, addressDto);
    }

    @PostMapping("/createAddress")
    @ResponseStatus(HttpStatus.OK)
    Long createAddress(@RequestBody AddressDto addressDto) {
        return addressService.createAddress(addressDto);
    }
}
