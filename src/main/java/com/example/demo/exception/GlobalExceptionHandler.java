package com.example.demo.exception;

import com.example.demo.model.exception.ErrModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import java.io.FileNotFoundException;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(value = MaxUploadSizeExceededException.class)
    public ResponseEntity<ErrModel> fileUploadSizeExceededException(MaxUploadSizeExceededException ex) {
        var errModel = new ErrModel(HttpStatus.CONFLICT.value(), ex.getMessage());
        log.error(errModel.toString());
        return ResponseEntity
                .status(HttpStatus.CONFLICT.value())
                .body(errModel);
    }

    @ExceptionHandler(value = FileNotFoundException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ErrModel> ErrorResponseHandleCustomerAlreadyExistsException(
            FileNotFoundException ex) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("File-Empty", "ATL");
        return ResponseEntity.status(HttpStatus.CONFLICT).headers(headers)
                .body(new ErrModel(HttpStatus.CONFLICT.value(), ex.getMessage()));
    }
}
