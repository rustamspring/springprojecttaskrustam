package com.example.demo.exception;


public class FileNotFoundException extends RuntimeException {

    private String message;

    public FileNotFoundException() {
    }

    public FileNotFoundException(String message) {
        super(message);
        this.message = message;
    }
}



