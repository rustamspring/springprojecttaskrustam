package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.sql.SQLOutput;

@Configuration
public class AppConfig {

    @Bean("getName")
    @Profile("local")
    public String getName(){
        System.out.println("LOCAL");
        return null;
    }
    @Bean("getName")
    @Profile("test")
    public String getName2(){
        System.out.println("TEST");
        return null;
    }
}
